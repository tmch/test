<?php

namespace App\Infrastructure\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ProductPersistenceObject
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=191)
     */
    private $id;

    /**
     * Наименование товара.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=191)
     */
    private $title;

    /**
     * Артикул.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=191)
     */
    private $code;

    public function __construct(string $id, string $title, string $code)
    {
        $this->id = $id;
        $this->title = $title;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
